/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package examen.U1.ExamenU1.Repository;

import examen.U1.ExamenU1.Model.AlumnoModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Niko
 */
public interface AlumnoRepository extends JpaRepository<AlumnoModel,Integer>{
    
}
