/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package examen.U1.ExamenU1.Model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Niko
 */
@Entity
@Table(name = "alumno")
public class AlumnoModel implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)    
    private Integer numero_control;
    private char genero;
    private double medida_cintura;
    private double medida_altura;
    
    
    @Transient
    private double ica;
    @Transient
    private String nivel;
    
    public AlumnoModel() {
    }


    public AlumnoModel(char genero, double medida_cintura, double medida_altura) {
        
        this.genero = genero;
        this.medida_cintura = medida_cintura;
        this.medida_altura = medida_altura;
    }
    
    

    public Integer getNumero_control() {
        return numero_control;
    }

    public void setNumero_control(Integer numero_control) {
        this.numero_control = numero_control;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public double getMedida_cintura() {
        return medida_cintura;
    }

    public void setMedida_cintura(double medida_cintura) {
        this.medida_cintura = medida_cintura;
    }

    public double getMedida_altura() {
        return medida_altura;
    }

    public void setMedida_altura(double medida_altura) {
        this.medida_altura = medida_altura;
    }
    
    public double getIca() {
        return this.medida_cintura/medida_altura;
    }

    public void setIca(Integer edad) {
        this.ica = ica;
    }
    
    public String getNivel() {
        String niv ="";
        if(this.genero== 'H' && getIca()<=0.34){
        niv= "Delgadez severa";
        }
        if(this.genero== 'H' && getIca()>=0.35 && getIca()<=0.42){
        niv= "Delgadez leve";
        }
        if(this.genero== 'H' && getIca()>=0.43 && getIca()<=0.52){
        niv= "Peso normal";
        }
        if(this.genero== 'H' && getIca()>=0.53 && getIca()<=0.57){
        niv= "Sobrepeso";
        }
        if(this.genero== 'H' && getIca()>=0.58 && getIca()<=0.62){
        niv= "Sobrepeso elevado";
        }
        if(this.genero== 'H' && getIca()>=0.63){
        niv= "Obesidad mórbida";
        }
        
        
        if(this.genero== 'M' && getIca()<=0.34){
        niv= "Delgadez severa";
        }
        if(this.genero== 'M' && getIca()>=0.35 && getIca()<=0.41){
        niv= "Delgadez leve";
        }
        if(this.genero== 'M' && getIca()>=0.42 && getIca()<=0.48){
        niv= "Peso normal";
        }
        if(this.genero== 'M' && getIca()>=0.49 && getIca()<=0.53){
        niv= "Sobrepeso";
        }
        if(this.genero== 'M' && getIca()>=0.54 && getIca()<=0.57){
        niv= "Sobrepeso elevado";
        }
        if(this.genero== 'M' && getIca()>=0.58){
        niv= "Obesidad mórbida";
        }
        
        return niv;
    }

    public void setNivel(Integer edad) {
        this.nivel = nivel;
    }
}
