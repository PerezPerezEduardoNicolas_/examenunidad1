/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package examen.U1.ExamenU1.Controller;

import examen.U1.ExamenU1.Model.AlumnoModel;
import examen.U1.ExamenU1.Service.AlumnoService;
import examen.U1.ExamenU1.Utils.CustomeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Niko
 */
@RestController
@RequestMapping("/api/v1/alumno")
public class AlumnoController {
     @Autowired
    private AlumnoService alumnoService;
    
     @PostMapping("/")
    public CustomeResponse createAlumno(@RequestBody AlumnoModel alumno) {
        
        CustomeResponse customeResponse = new CustomeResponse();
        alumnoService.createAlumno(alumno);        
        return customeResponse;
    }
    
    @GetMapping("/")
    public CustomeResponse getAlumno(){
        CustomeResponse customeResponse = new CustomeResponse();
        customeResponse.setData(alumnoService.getAlumno());
        return customeResponse;
    }
    
    @GetMapping("/respuesta")
    public CustomeResponse getRespuesta(){
        CustomeResponse customeResponse = new CustomeResponse();
        
        
        customeResponse.setData(alumnoService.getAlumno());
        return customeResponse;
    }
    

    @PutMapping("/{numero_control}")
    public CustomeResponse updateFactura(@RequestBody AlumnoModel alumno, @PathVariable Integer numero_control){
        CustomeResponse customeResponse = new CustomeResponse();
        alumnoService.updateAlumno(alumno, numero_control);        
        return customeResponse;       
    
    }
    
    @DeleteMapping("/{numero_control}")
    public CustomeResponse deleteAlumno(@PathVariable Integer numero_control){
        CustomeResponse customeResponse = new CustomeResponse();
        alumnoService.deleteAlumno(numero_control);        
        return customeResponse;       
    }
    
    
    
}
